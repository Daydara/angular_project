import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {MangaComponent} from './livre/manga/manga.component';
import {ComicComponent} from './livre/comic/comic.component';
import {HomeComponent} from './livre/home/home.component';
import {NotFoundComponent} from './not-found/not-found.component';


const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  {path: 'books', loadChildren: './livre/livre.module#LivreModule' },
  {path: 'not-found', component: NotFoundComponent },
  {path: '**', redirectTo: 'not-found'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true, enableTracing: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
