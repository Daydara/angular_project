import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LivreRoutingModule } from './livre-routing.module';
import { MangaComponent } from './manga/manga.component';
import { ComicComponent } from './comic/comic.component';

@NgModule({
  declarations: [MangaComponent, ComicComponent],
  imports: [
    CommonModule,
    LivreRoutingModule
  ]
})
export class LivreModule { }
