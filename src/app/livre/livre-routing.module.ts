import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ComicComponent} from './comic/comic.component';
import {MangaComponent} from './manga/manga.component';

const routes: Routes = [
  {path: 'comics', component: ComicComponent },
  {path: 'mangas', component: MangaComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LivreRoutingModule { }
