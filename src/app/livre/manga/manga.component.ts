import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-manga',
  templateUrl: './manga.component.html',
  styleUrls: ['./manga.component.scss']
})
export class MangaComponent {
  MangaList = [
    { name: 'Jojo', console: 'PS4' },
    { name: 'Berserk', console: 'PS4' },
  ];

}
